import { Component, OnInit } from '@angular/core';
import {AuthService} from "../service/auth.service";
import {error} from "@angular/compiler-cli/src/transformers/util";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  form: any = {
    username:null,
    email: null,
    password: null
  };
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';


  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

  onSubmit(): void{

    const {username, password, email} = this.form;

    this.authService.register(username,email,password).subscribe({
      next: data =>{
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
      },
      error: error =>{
        this.errorMessage = error.error.message;
        this.isSignUpFailed = true;
      }
    })
  }

}
