package com.lc.shop.dao.impl;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Repository;

import com.lc.shop.dao.AbstractDAO;
import com.lc.shop.dao.UsersDAO;
import com.lc.shop.en.SessoEn;
import com.lc.shop.model.Users;


@Repository
public class UsersDaoImpl extends AbstractDAO<Users, Long> implements UsersDAO{

	@Override
	public void updateUsers(Users user) {

		
	}

	@Override
	public List<Users> selectAll() {
		return super.selectAll();
	}

	@Override
	public Users selectByCodiceIndividuale(String codiceIndividuale) {
		String jpql = "SELECT u FROM Users u WHERE u.codiceIndividuale like :codiceIndividuale";

		Users users = (Users) entityManager.createQuery(jpql).setParameter("codiceIndividuale",codiceIndividuale).getSingleResult();

		return users;
	}

	@SuppressWarnings("unchecked")
	public List<Users> selectBySesso(SessoEn sesso) {
		
		String jpql = "SELECT u FROM Users u WHERE u.sesso like :sesso";
		
		List<Users> users = entityManager.createQuery(jpql).setParameter("sesso", sesso.toString()).getResultList();
		
		return users;
		
	}
	
	public void insert(Users user) {	
		insert(user);
	}

//	@Override
//	public Users selectByName(String name) {
//		
//		CriteriaBuilder queryBuilder = entityManager.getCriteriaBuilder();
//		CriteriaQuery<Users> querydefinition = queryBuilder.createQuery(Users.class);
//		
//		Root<Users> rs = querydefinition.from(this.entityClass);
//		
//		querydefinition.select(rs).where(queryBuilder.equal(rs.get("nome"), name));
//		
//		Users user = entityManager.createQuery(querydefinition).getSingleResult();
//		entityManager.clear();
//		
//		return user;
//	}
	
	
	/*
	 * Creazione query con hibernate Criteria
	 * query in chiaro
	 * 
	 * SELECT* 
	 * FROM USERS 
	 * WHERE 'nome'+'cognome' like %nominativo% 
	 * or 'cognome'+'nome' like %nominativo%
	 * 
	 * */
	@Override
	public List<Users> selectByNominativo(String nominativo) {
		
		CriteriaBuilder queryBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Users> querydefinition = queryBuilder.createQuery(Users.class);
		
		Root<Users> rs = querydefinition.from(this.entityClass);
		
		Expression<String> exp1 = queryBuilder.concat(rs.<String>get("nome"), "");
		exp1 = queryBuilder.concat(exp1, rs.<String>get("cognome"));
		
		Expression<String> exp2 = queryBuilder.concat(rs.<String>get("cognome"), "");
		exp2 = queryBuilder.concat(exp2, rs.<String>get("nome"));
		
		String toSearch = "%" + nominativo + "%" ;
		
		Predicate whereClause = queryBuilder.or(
				queryBuilder.like(exp1, toSearch),
				queryBuilder.like(exp2, toSearch));
		
		querydefinition.select(rs).where(whereClause);
		
		List<Users> users = entityManager.createQuery(querydefinition).getResultList();
		
		entityManager.clear();
			
		return users;
		
		
	}

}
