package com.lc.shop.jpa;

import java.io.Serializable;
import java.util.List;

import com.sun.istack.NotNull;


public interface GenericRepositorty<I extends Serializable,E extends Serializable>{
	
	@NotNull
	List<I> selectAll();
	void insert(@NotNull I entity);
	void update(@NotNull I entity);
	I selectById(@NotNull E id);
	void delete(@NotNull I entity);
	void deleteById(@NotNull E id);
	
	
	
}
