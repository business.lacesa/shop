package com.lc.shop.en;

public enum RoleEn {
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}