package com.lc.shop.jpa;

import com.lc.shop.en.SessoEn;
import com.lc.shop.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UsersRepository extends JpaRepository<Users, Long> {


    Optional<Users> findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    Users findUsersById(Long id);

    List<Users> findAll();

    List<Users> findUsersBySesso(SessoEn sesso);

    List<Users> findUsersByNomeAndCognome(String nome,String cognome);
}
