package com.lc.shop.dao;

import java.util.List;

import com.lc.shop.en.SessoEn;
import com.lc.shop.model.Users;

public interface UsersDAO {

	Users selectById(Long id);

	List<Users> selectAll();

	Users selectByCodiceIndividuale(String codInd);
	
	List<Users> selectBySesso(SessoEn sesso);
	
	void updateUsers(Users user);

	List<Users> selectByNominativo(String nominativo);
	
}
