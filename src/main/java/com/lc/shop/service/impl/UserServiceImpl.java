package com.lc.shop.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import com.lc.shop.jpa.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.lc.shop.en.SessoEn;
import com.lc.shop.model.Users;
import com.lc.shop.service.UsersService;

@Service("UsersService")
@Transactional
public class UserServiceImpl implements UsersService, UserDetailsService {

	@Autowired
	private UsersRepository usersRepository;
	
//	/*Per ora riportiamo solo i metodi dello Strato del DAO,
//	 * ma nel service noi andiamo a manipolare i dati che ci ritornano dal dao
//	 * per adattarli a quello che ci serve nel client
//	 */
//
//	@Override
//	public Users selectById(Long id) {
//		return usersRepository.selectById(id);
//	}
//
//	@Override
//	public Users selectByCodiceIndividuale(String codInd) {
//		return usersRepository.selectByCodiceIndividuale(codInd);
//	}
//
//	@Override
//	public List<Users> selectAll() {
//		return usersRepository.selectAll();
//	}
//
//	@Override
//	public List<Users> selectBySesso(SessoEn sesso) {
//		return usersRepository.selectBySesso(sesso);
//	}
//
//	@Override
//	public void updateUsers(Users user) {
//
//		//implementare la query nello strato del DAO
//
//	}
//
//	@Override
//	public List<Users> selectByNominativo(String nominativo) {
//		return usersRepository.selectByNominativo(nominativo);
//	}

	@Override
	public Users findUsersById(Long id) {
		return usersRepository.findUsersById(id);
	}

	@Override
	public List<Users> findAll() {
		return usersRepository.findAll();
	}

	@Override
	public List<Users> findUsersBySesso(SessoEn sesso) {
		return usersRepository.findUsersBySesso(sesso);
	}

	@Override
	public void updateUsers(Users user) {

	}

	@Override
	public List<Users> findUsersByNomeAndCognome(String nome, String cognome) {
		return usersRepository.findUsersByNomeAndCognome(nome, cognome);
	}

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Users user = usersRepository.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));

		return UserDetailsImpl.build(user);
	}

}
