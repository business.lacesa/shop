package com.lc.shop.model;

import com.lc.shop.en.RoleEn;

import javax.persistence.*;

@Entity
@Table(name = "Roles")
public class Roles {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idRole")
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(name = "roleDesc")
    private RoleEn name;

    public Roles() {

    }

    public Roles(RoleEn name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public RoleEn getName() {
        return name;
    }

    public void setName(RoleEn name) {
        this.name = name;
    }
}