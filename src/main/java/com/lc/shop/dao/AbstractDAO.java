package com.lc.shop.dao;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import com.lc.shop.jpa.GenericRepositorty;

public abstract class AbstractDAO<I extends Serializable, Id extends Serializable> implements GenericRepositorty<I, Id> {
	
	@PersistenceContext
	protected EntityManager entityManager;
	protected final Class<I> entityClass;
	CriteriaBuilder builder;
	
	@SuppressWarnings(value = { "unchecked" })
	public AbstractDAO() {
		this.entityClass =(Class<I>) 
				((ParameterizedType)this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	private CriteriaQuery<I> initCryteria(){
		
		builder = this.entityManager.getCriteriaBuilder();
		return builder.createQuery(this.entityClass);
	}
	
	public List<I> selectAll() {
		CriteriaQuery<I> query = initCryteria();
		
		return this.entityManager.createQuery(
				query.select(
						query.from(this.entityClass))).getResultList();
	}
	
	public I selectById(Id id) {
		
		CriteriaQuery<I> query = initCryteria();
		return this.entityManager.createQuery(query.where(builder.equal(query.from(this.entityClass).get("id"), id))).getSingleResult();
		
	}
	
	public void insert(I entity) {
		
		this.entityManager.persist(entity);
		flushAndClear();
	}
	

	public void update(I entity) {
	
		this.entityManager.merge(entity);
		flushAndClear();
	}
	
	public void delete(I entity) {
		
		this.entityManager.merge(this.entityManager.contains(entity) ? entity:this.entityManager.merge(entity));
		flushAndClear();
	}
	
	public void deleteById(Id id) {
		
		CriteriaQuery<I> query = initCryteria();
		this.entityManager.createQuery(query.where(builder.equal(query.from(this.entityClass).get("id"), id))).executeUpdate();
	}
	
	
	//da vedere se serve l'updatebyid
	
	private void flushAndClear() {
		entityManager.flush();
		entityManager.clear();
		
	}
	
	

}
