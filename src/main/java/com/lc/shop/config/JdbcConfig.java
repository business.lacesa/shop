package com.lc.shop.config;

import javax.sql.DataSource;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mchange.v2.c3p0.DriverManagerDataSource;

@Configuration
@EnableTransactionManagement
@ComponentScan({"com.lc.shop.config"})
@PropertySource("classpath:application.properties")
public class JdbcConfig {
	
	@Autowired
	private Environment env;

	@Bean
	public JdbcTemplate jdbcTemplate(DataSource dataSource)
	{
		return new JdbcTemplate(dataSource);
	}

	@Bean
	public NamedParameterJdbcTemplate getJdbcTemplate(DataSource datasource) {
		return new NamedParameterJdbcTemplate(datasource);
	};
	
	@Bean(name = "dataSource")
	public DataSource datasource() {
		DriverManagerDataSource datasource = new DriverManagerDataSource();
		
		datasource.setDriverClass(env.getRequiredProperty("spring.datasource.driver-class-name"));
		datasource.setJdbcUrl(env.getRequiredProperty("spring.datasource.url"));
		datasource.setUser(env.getRequiredProperty("spring.datasource.username"));
		datasource.setPassword(env.getRequiredProperty("spring.datasource.password"));
		
		return datasource;
	};
	
	
	@Bean
	public DataSourceTransactionManager transactionManager() {

		DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
		transactionManager.setDataSource(datasource());

		return transactionManager;

	}

}
