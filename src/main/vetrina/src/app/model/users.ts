import {DatePipe} from "@angular/common";

export class Users {
  userId: number;
  nome: string;
  cognome:string;
  dataDiNascita: DatePipe;
  sesso: string;
  codiceIndividuale: string;
}
