package com.lc.shop.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.lc.shop.en.RoleEn;
import com.lc.shop.en.SessoEn;
import org.apache.catalina.User;

@Entity
@Table(name="Users")
public class Users implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "userId", unique=true, nullable = false)
	private Long id;
	
	@Column(name = "nome")
	private String nome;
	
	@Column(name="cognome")
	private String cognome;
	
	@Column(name="dataDiNascita")
	private Date dataDiNascita;
	
	@Column(name="sesso")
	@Enumerated(EnumType.STRING)
	private SessoEn sesso;

	@NotBlank
	@Column(name="password")
	private String password;

//	@NotBlank
	@Column(name="username")
	private String username;

//	@NotBlank
	@Email
	@Column(name="email")
	private String email;
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(  name = "UserRole",
			joinColumns = @JoinColumn(name = "userId"),
			inverseJoinColumns = @JoinColumn(name = "roleId")
	)
	private Set<Roles> roles = new HashSet<>();


	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Users(){}

	public Users(String username,String email, String password){
		this.username = username;
		this.email = email;
		this.password = password;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public Date getDataDiNascita() {
		return dataDiNascita;
	}

	public void setDataDiNascita(Date dataDiNascita) {
		this.dataDiNascita = dataDiNascita;
	}

	public SessoEn getSesso() {
		return sesso;
	}

	public void setSesso(SessoEn sesso) {
		this.sesso = sesso;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<Roles> getRoles() {
		return roles;
	}

	public void setRoles(Set<Roles> roles) {
		this.roles = roles;
	}
}
