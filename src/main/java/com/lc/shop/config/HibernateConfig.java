package com.lc.shop.config;

import java.util.Properties;
import javax.persistence.SharedCacheMode;
import javax.persistence.ValidationMode;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan({"com.lc.shop.config"})
@PropertySource("classpath:application.properties")
public class HibernateConfig {

	@Autowired
	private Environment env;
	
	@Autowired
	private DataSource dataSource;
	
	@Bean
	LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setJpaVendorAdapter(this.jpaVendorAdapter());
		factory.setDataSource(this.dataSource);
		factory.setPackagesToScan("com.lc.shop.model");
		factory.setJpaProperties(this.hibernateProperties());
		factory.setSharedCacheMode(SharedCacheMode.ENABLE_SELECTIVE);
		factory.setValidationMode(ValidationMode.NONE);
		return factory;
		
		
	}

	private Properties hibernateProperties() {
		Properties jpaProp = new Properties();
		
		jpaProp.put("javax.persistance.schema-generation.database.action", "none");
		jpaProp.put("hibernate.dialect", env.getRequiredProperty("hibernate.dialect"));
		jpaProp.put("hibernate.show_sql", env.getRequiredProperty("hibernate.show_sql"));
		jpaProp.put("hiberante.format", env.getRequiredProperty("hiberante.format"));
		
		jpaProp.put("hibernate.c3p0.min_size", env.getRequiredProperty("hibernate.c3p0.min_size"));
		jpaProp.put("hibernate.c3p0.max_size", env.getRequiredProperty("hibernate.c3p0.max_size"));
		jpaProp.put("hibernate.c3p0.acquire_incremet", env.getRequiredProperty("hibernate.c3p0.acquire_incremet"));
		jpaProp.put("hibernate.c3p0.timeout", env.getRequiredProperty("hibernate.c3p0.timeout"));
		jpaProp.put("hibernate.c3p0.max_statements", env.getRequiredProperty("hibernate.c3p0.max_statements"));
		
		return jpaProp;
	}

	private JpaVendorAdapter jpaVendorAdapter() {
		HibernateJpaVendorAdapter hjva = new HibernateJpaVendorAdapter();
		hjva.setShowSql(true);
		hjva.setGenerateDdl(false);
		hjva.setDatabasePlatform(env.getRequiredProperty("hibernate.dialect"));
		return hjva;
	}
	
}
