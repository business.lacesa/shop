package com.lc.shop.jpa;

import java.util.Optional;

import com.lc.shop.en.RoleEn;
import com.lc.shop.model.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RolesRepository extends JpaRepository<Roles, Long> {
    Optional<Roles> findByName(RoleEn name);
}
