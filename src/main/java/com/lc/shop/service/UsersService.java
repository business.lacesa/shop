package com.lc.shop.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.lc.shop.en.SessoEn;
import com.lc.shop.model.Users;

public interface UsersService  {

	Users findUsersById(Long id);

	List<Users> findAll();

	List<Users> findUsersBySesso(SessoEn sesso);

	void updateUsers(Users user);

	List<Users> findUsersByNomeAndCognome(String nome, String cognome);

	public UserDetails loadUserByUsername(String username);
	
}
